package consoleapp;
/**
 * Project: VCFContacts
 * File: ConsoleApp.java
 * Version: 1.0.0
 * Date: 28. 6. 2017
 * Author: Jakub Majzl�k  
 */

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import main.Contact;
import main.ContactReader;

public class ConsoleApp
{
	
	ContactReader reader;
	
	ArrayList<Contact> list;
		
	static Scanner s = new Scanner(System.in);
	
	public ConsoleApp()
	{
		reader = new ContactReader();
	}
	
	public void run()
	{
		String path;
		
		System.out.println("Enter the path to vcf file with contacts.");
		path = s.nextLine();
		
		reader.readContacts(path);
		list = new ArrayList<Contact>(reader.getContactList());
		
		showContacts();
	}
	
	void showContacts()
	{
		int numberOfContacts = list.size();
		int contactsOnPage = 20;
		int currentPage = 1;
		String operation;
		
		while(true)
		{
			for (int i = (currentPage-1) * contactsOnPage; i < currentPage * contactsOnPage; i++)
			{
				if(i < list.size())
				{
					System.out.println(list.get(i).name + " " + list.get(i).number);
				}
			}
			
			if(currentPage > 1)
			{
				System.out.print("<< A   ");
			}
			
			if(currentPage <= (numberOfContacts / contactsOnPage))
			{
				System.out.print("   D >>");
			}
			
			System.out.println("   Q to quit");
			
			operation = s.next();
			
			switch(operation)
			{
				case "a": case "A":
					if(currentPage > 1)
					{
						currentPage--;
					}
					break;
					
				case "d": case "D":
					if(currentPage <= (numberOfContacts / contactsOnPage))
					{
						currentPage++;
					}
					break;
				case "q": case "Q":
					return;
				default: 
					break;
			}
			
			try
			{
				Runtime.getRuntime().exec("cls");
			}
			catch (IOException e)
			{
			}
			
		}
		
		
	}

	public static void main(String[] args)
	{
		ConsoleApp app = new ConsoleApp();
		app.run();
	}
}
