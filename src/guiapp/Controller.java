/**
 * Project: VCFContacts
 * File: StartController.java
 * Version: 1.0
 * Date: 11. 7. 2017
 * Author: Jakub Majzlík
 */
package guiapp;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import main.Contact;
import main.ContactReader;

/**
 * Class which control gui components
 */
public class Controller implements Initializable
{

    private ContactReader contactReader = new ContactReader();

	@FXML 
	private ListView<String> contactsListView;

	@FXML
	private TextField nameTextField;

	@FXML
	private TextField numberTextField;

	@FXML
    private TextField searchbarTextField;

	/* (non-Javadoc)
	 * @see javafx.fxml.Initializable#initialize(java.net.URL, java.util.ResourceBundle)
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources)
	{
	}

	@FXML
	public void chooseFile() throws Exception
	{
		//Initializing file chooser
		FileChooser fileChooser = new FileChooser();
		FileChooser.ExtensionFilter extensionFilter = new FileChooser
				.ExtensionFilter("VCF files (*.vcf)", "*.vcf");
		fileChooser.getExtensionFilters().add(extensionFilter);
		File file;
		file = fileChooser.showOpenDialog(new Stage());

		if (file != null)
		{
			contactReader.readContacts(file);		
			
			ObservableList<String> items = FXCollections.observableArrayList();
			
			for(int i = 0; i < contactReader.getContactList().size(); i++)
			{				
				String item = contactReader.getContactList().get(i).name;
				items.add(item);
			}

			//Setting contacts to ListView
			contactsListView.getItems().setAll(items);

			//Handling click events on items in ListView
			contactsListView.setOnMouseClicked(event ->
			{
                Contact clickedContact = contactReader.findContact(contactsListView.getSelectionModel().getSelectedItem());
                //Setting contact view
                nameTextField.setText(clickedContact.name);
                numberTextField.setText(clickedContact.number.substring(1));
            });
		}
	}

	@FXML
    public void filterContacts()
    {
        if (searchbarTextField.getText() != null)
        {
            List<Contact> filteredList = new ArrayList<>();

            // **********SOLVE -1 PROBLEM PLS*********
			//Finding contacts which satisfying the filter
            for(int i = 0; i < contactReader.getContactList().size()-1; i++)
            {
                if (contactReader.getContactList().get(i).name.toLowerCase()
                        .contains(searchbarTextField.getText().toLowerCase()))
                {
                    filteredList.add(contactReader.getContactList().get(i));
                }
            }

            //Updating ListView with new items
            ObservableList<String> items = FXCollections.observableArrayList();

			for (Contact aFilteredList : filteredList) {
				String item = aFilteredList.name;
				items.add(item);
			}

            contactsListView.getItems().setAll(items);
        }
    }
}
