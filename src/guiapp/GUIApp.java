/**
 * Project: VCFContacts
 * File: GUIApp.java
 * Version: 1.0
 * Date: 7. 7. 2017
 * Author: Jakub Majzlík
 */
package guiapp;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class GUIApp extends Application
{

	/*
	 * (non-Javadoc)
	 * 
	 * @see javafx.application.Application#start(javafx.stage.Stage)
	 */
	@Override
	public void start(Stage primaryStage) throws Exception
	{
		//Initializing the window
		Parent root = FXMLLoader.load(getClass().getResource("contacts_screen.fxml"));
		primaryStage.setTitle("VCF Contact reader");
		primaryStage.setResizable(false);
		primaryStage.initStyle(StageStyle.UTILITY);
		primaryStage.setScene(new Scene(root, 800, 400));
		primaryStage.show();
	}

	

	/**
	 * @param args program arguments
	 */
	public static void main(String[] args)
	{
		launch(args);

	}

}
