/**
 * Project: VCFContacts
 * File: ContactReader.java
 * Version: 1.0.0
 * Date: 15. 6. 2017
 * Author: Jakub Majzlík
 */
package main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Class which have all necessary funcions for working with vcf file.
 */
public class ContactReader
{
	private List<Contact> contactList = new ArrayList<>();

	/**
	 * Function which reads contacts informations from file and stores them
	 * to ArrayList contactList.
	 * @param file File from which contacts will be readed.
	 */
	public void readContacts(File file)
	{
		readContacts(file.getAbsolutePath());
	}

	/**
	 * Function which reads contacts informations from file which path is as parameter
	 * and stores them to AraryList contactList.
	 * @param path Path to file from which contacts will be readed.
	 */
	public void readContacts(String path)
	{

		BufferedReader br = null;

		try
		{
			String name, number, tempString;
			boolean readingContact = false;
			int numberOfNumbers = 0;
			br = new BufferedReader(new FileReader(path));

			while ((tempString = br.readLine()) != null)
			{
				//Start of contact
				if (tempString.startsWith("BEGIN"))
				{
					if (readingContact)
					{
						System.out.println("Error: Wrong file format");
						return;
					}

					readingContact = true;
					contactList.add(new Contact());
					continue;
				}

				//End of contact
				if (tempString.startsWith("END"))
				{
					readingContact = false;
					continue;
				}

				//Contact's data
				if (readingContact)
				{

					//Contact's name
					if (tempString.startsWith("FN"))
					{
						// 3 is name's start index => "FN:Name"
						// 0123
						name = tempString.substring(3);

						contactList.get(numberOfNumbers).name = name;
						continue;
					}

					//Contact's number
					if (tempString.startsWith("TEL"))
					{
						int startOfNumber = tempString.indexOf(":");

						number = tempString.substring(startOfNumber);

						contactList.get(numberOfNumbers).number = number;

						numberOfNumbers++;
					}
				}
			}
		}
		catch (Exception e)
		{
			System.out.println("Error: Cannot open the file");
		}
		finally
		{
			if (br != null)
			{
				//Closing the file
				try
				{
					br.close();
				}
				catch (IOException e)
				{
					System.out.println("Cannot close file");
				}
			}
		}
	}

	/**
	 * Function which get List witch contacts.
	 * @return List with contacts
	 */
	public List<Contact> getContactList(){ return contactList;}

	/**
	 * Function which find contact and return it. If the contact was not found,
	 * return null.
	 * @param name Name of contact to find.
	 * @return Contact
	 */
	public Contact findContact(String name)
	{
		for (Contact contact : contactList)
		{
			if(contact.name.equals(name))
			{
				return contact;
			}
		}
		return null;
	}

}
