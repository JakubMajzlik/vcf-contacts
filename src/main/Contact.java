package main;
/**
 * Project: VCFContacts
 * File: Contact.java
 * Version: 1.0.0
 * Date: 15. 6. 2017
 * Author: Jakub Majzl�k  
 */

public class Contact
{
	public String number;
	public String name;
}
